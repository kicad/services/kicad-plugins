from starlette.routing import Mount, Route, Router
from fastapi.responses import RedirectResponse, FileResponse
from urllib.parse import urljoin

from app.config import BASE_PCM_SOURCE
from .api import api_routes
from .pcm import pcm as pcm_routes


async def repojson(request):
    url = urljoin(BASE_PCM_SOURCE, "repository.json")
    return RedirectResponse(url, status_code=307)


async def packagesjson(request):
    url = urljoin(BASE_PCM_SOURCE, "packages.json")
    return RedirectResponse(url, status_code=307)


async def pcmhtml(request):
    return FileResponse("static/pcm.html")


# mounted app can also be an instance of `Router()`
routes = Router([
    Route("/repository.json", endpoint=repojson),
    Route("/packages.json", endpoint=packagesjson),
    Mount('/api', app=api_routes),
    Mount('/pcm', app=pcm_routes),
    Route("/index.html", endpoint=pcmhtml),
    Route("/", endpoint=pcmhtml),
])
