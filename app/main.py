from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from app.routes import routes
import app.config as config

application = FastAPI()
application.debug = config.DEBUG

application.mount('/static', StaticFiles(directory='static'), name='static')
application.mount('/', routes)
