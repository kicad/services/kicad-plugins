import requests
import jsonschema

SCHEMA = requests.get("https://go.kicad.org/pcm/schemas/v1").json()

REPOSCHEMA = {**SCHEMA, "$ref": "#/definitions/Repository"}
PACKAGESSCHEMA = {**SCHEMA, "$ref": "#/definitions/PackageArray"}

REPO_VALIDATOR = jsonschema.Draft7Validator(REPOSCHEMA)
PACKAGES_VALIDATOR = jsonschema.Draft7Validator(PACKAGESSCHEMA)
