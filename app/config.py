from starlette.config import Config
from starlette.datastructures import URL, Secret

# Config will be read from environment variables and/or ".env" files.
_config = Config(".env")

DEBUG = _config('DEBUG', cast=bool, default=False)
TESTING = _config('TESTING', cast=bool, default=False)
DATABASE_URL = _config('DATABASE_URL', default='')

BASE_PCM_SOURCE = _config(
    'BASE_PCM_SOURCE', default='https://gitlab.com/kicad/addons/repository/-/raw/main/')

PCM_WEB_CLEANUP_CHANCE = _config(
    'PCM_WEB_CLEANUP_CHANCE', cast=float, default=0.01)
PCM_WEB_MAX_DOWNLOAD_SIZE = _config(
    'PCM_WEB_MAX_DOWNLOAD_SIZE', cast=int, default=10 * 1024 * 1024)
PCM_WEB_MAX_REPO_SIZE = _config(
    'PCM_WEB_MAX_REPO_SIZE', cast=int, default=64 * 1024)
PCM_WEB_MAX_PACKAGES_SIZE = _config(
    'PCM_WEB_MAX_PACKAGES_SIZE', cast=int, default=10 * 1024 * 1024)
PCM_WEB_CACHE_FRESH_TIME = _config(
    'PCM_WEB_CACHE_FRESH_TIME', cast=int, default=5 * 60)
PCM_WEB_CACHE_LIFE_TIME = _config(
    'PCM_WEB_CACHE_LIFE_TIME', cast=int, default=24 * 60 * 60)
