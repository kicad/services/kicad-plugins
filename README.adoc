== Local Setup

kicad-plugins utilizes https://python-poetry.org/[poetry] to handle dependencies and ensure reproducible builds.

=== Usage with VSCode
After cloning the repo and opening it in VSCode.

You should execute `poetry install` to install dependencies and
`poetry shell` to trigger the creation of a poetry venv for the project.

After the venv is created, you need to switch VSCode to use the poetry created virtualenv as the interpreter.

By default poetry creates the virtualenv in central location in user directory. If you want it to be in
`{workspaceDir}/.venv` then set https://python-poetry.org/docs/configuration/#virtualenvsin-project[virtualenvs.in-project] config variable to `true`.

=== Adding dependencies

Simply execute `poetry add <package>`

It is highly recommended you pay attention to the versioning selector.

For example ^3.0.0 selects 3.0.0 to 4.0.0
~ selects 3.1.0 to <3.2.0

=== Updating/modifying dependencies

Modify the pyproject.toml declaration for a dependency and then execute

`poetry update <package>`
